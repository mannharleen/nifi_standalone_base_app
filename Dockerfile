FROM apache/nifi:1.9.0

USER root

# create an ec2-user group and add nifi user to it. This is to be able to run on Amazon Linux
RUN groupadd -g 500 ec2-user \
    && usermod -a -G ec2-user nifi
USER nifi

# Copy the flow.xml.gz from other nifi instance here and add it to the container
COPY --chown=nifi:nifi flow.xml.gz ${NIFI_HOME}/conf/

# Copy the logbackfile
COPY --chown=nifi:nifi logback.xml ${NIFI_HOME}/conf/

# Makes changes to start.sh to enable -e when using Docker run
# eg. adding the following line: 
# 				prop_replace 'java.arg.3' "${NIFI_JVM_Xmx:--Xmx512m}" "${NIFI_HOME}/conf/bootstrap.conf"
#     enables the use of -e NIFI_JVM_Xmx=-Xmx1g
#
COPY --chown=nifi:nifi start.sh ${NIFI_BASE_DIR}/scripts/

COPY ./jars /opt/jars/

# Copy file in which variable registry variables can be defined
COPY --chown=nifi:nifi custom.properties ${NIFI_HOME}/conf/

RUN ls -lrt ./conf/ && ls -lrt ../scripts/

# use this for ubuntu 
#USER nifi 

# use this for amazon linux ec2-user
#RUN groupadd -g 500 ec2-user \
    #&& useradd -r -u 500 -g 500 nifi-ec2-user
#USER nifi-ec2-user

ENTRYPOINT ["../scripts/start.sh", "& tail -f logs/nifi-app.log", "& whoami"]

# docker build -t hbv2_standalone_cnimeterdataapi .
## docker run with all env variables
# docker run -d --rm --name mynifi -p 8080:8080 -p 20000:20000 -e NIFI_JVM_Xmx=-Xmx5g -e JBCIMD_URL=zzz -e JBCIMD_USERNAME=zzz -e JBCIMD_PASSWORD=zzz -e JBCIMD_AWSACCESSKEY=zzz -e JBCIMD_AWSSECRETACCESSKEY=zzz hbv2_standalone_cnimeterdataapi
## docker run for DEV environment
# docker run -d --rm --name mynifi -p 8080:8080 -p 20000:20000 -e JBCIMD_PASSWORD=zzz JBCIMD_AWSSECRETACCESSKEY=zzz hbv2_standalone_cnimeterdataapi

