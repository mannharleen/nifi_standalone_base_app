# Base image for building a standalone NiFi application

## Context
Often enough we want to wrap NiFi flow into a stateless and standalone application, that can be started and stopped when required. This docker image is a base for creating such applications.

# dev guide


## Steps

- Install docker if not present
```
sudo snap install docker
```
- Configure aws keys if not present
```
aws configure
```
- git clone https://gitlab.com/mannharleen/nifi_standalone_base_app.git
- Edit the required files (see descirption of files below)
- Build the image
```
docker build -t [name] .
```
- test the image: 
```
sudo docker run -d --rm --name mynifi -p 8080:8080 -p 20000:20000 -e NIFI_JVM_Xmx=-Xmx512m [name]
```

- push the image to ECS repo
```
./buildscript.sh <version>
```

# devops

## Increase volume size available to nifi
ref: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html
```bash
### Check if there is a FS on the volume
sudo file -s /dev/nvme2n1
### If there is no filesystems on the volume, create one 
sudo mkfs -t xfs /dev/nvme2n1
### Finally mount the volume at the desired location 
sudo mount /dev/nvme2n1 /var/lib/docker/volumes
```

## Contact
If you need more information please contact me on - mannharleen@gmail.com
