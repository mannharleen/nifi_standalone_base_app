#!/bin/sh -e

#    Licensed to the Apache Software Foundation (ASF) under one or more
#    contributor license agreements.  See the NOTICE file distributed with
#    this work for additional information regarding copyright ownership.
#    The ASF licenses this file to You under the Apache License, Version 2.0
#    (the "License"); you may not use this file except in compliance with
#    the License.  You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

scripts_dir='/opt/nifi/scripts'

[ -f "${scripts_dir}/common.sh" ] && . "${scripts_dir}/common.sh"

# Establish baseline properties
prop_replace 'nifi.web.http.port'               "${NIFI_WEB_HTTP_PORT:-8080}"
prop_replace 'nifi.web.http.host'               "${NIFI_WEB_HTTP_HOST:-$HOSTNAME}"
prop_replace 'nifi.remote.input.host'           "${NIFI_REMOTE_INPUT_HOST:-$HOSTNAME}"
prop_replace 'nifi.remote.input.socket.port'    "${NIFI_REMOTE_INPUT_SOCKET_PORT:-10000}"
prop_replace 'nifi.remote.input.secure'         'false'
prop_replace 'java.arg.3'                       "${NIFI_JVM_Xmx:--Xmx5g}"     "${NIFI_HOME}/conf/bootstrap.conf"
prop_replace 'nifi.variable.registry.properties'	"${NIFI_HOME}/conf/custom.properties"

# setting environment
prop_replace 'custom.properties.env'                "${ENV:-dev}"                               "${NIFI_HOME}/conf/custom.properties"

# setting some dev env properies (nothing sensitive)
prop_replace 'custom.properties.jbcimd_url'		"${JBCIMD_URL:-jdbc:postgresql://j17-prod-pguat01.cluster-ro-ckufkpgrfz0b.ap-southeast-2.rds.amazonaws.com:8080/jindabyne?sslmode=allow}"	"${NIFI_HOME}/conf/custom.properties"
prop_replace 'custom.properties.jbcimd_username'		"${JBCIMD_USERNAME:-harleenmann}"				"${NIFI_HOME}/conf/custom.properties"
prop_replace 'custom.properties.jbcimd_password'		"${JBCIMD_PASSWORD:-john_snow}"				"${NIFI_HOME}/conf/custom.properties"
prop_replace 'custom.properties.jbcimd_driver_class_name'	"${JBCIMD_DRIVERCLASSNAME:-org.postgresql.Driver}"	"${NIFI_HOME}/conf/custom.properties"

# setting some dev env properies (nothing sensitive)
prop_replace 'custom.properties.aws_access_key'		"${JBCIMD_AWSACCESSKEY:-AKIAI4RZ2J2RCZ542SIQ}"	"${NIFI_HOME}/conf/custom.properties"
prop_replace 'custom.properties.aws_secret_access_key'	"${JBCIMD_AWSSECRETACCESSKEY:-john_snow}"	"${NIFI_HOME}/conf/custom.properties"

# Check if we are secured or unsecured
case ${AUTH} in
    tls)
        echo 'Enabling Two-Way SSL user authentication'
        . "${scripts_dir}/secure.sh"
        ;;
    ldap)
        echo 'Enabling LDAP user authentication'
        # Reference ldap-provider in properties
        prop_replace 'nifi.security.user.login.identity.provider' 'ldap-provider'
        prop_replace 'nifi.security.needClientAuth' 'WANT'

        . "${scripts_dir}/secure.sh"
        . "${scripts_dir}/update_login_providers.sh"
        ;;
    *)
        if [ ! -z "${NIFI_WEB_PROXY_HOST}" ]; then
            echo 'NIFI_WEB_PROXY_HOST was set but NiFi is not configured to run in a secure mode.  Will not update nifi.web.proxy.host.'
        fi
        ;;
esac

# Continuously provide logs so that 'docker logs' can    produce them
tail -F "${NIFI_HOME}/logs/nifi-app.log" &
"${NIFI_HOME}/bin/nifi.sh" run &
nifi_pid="$!"

trap "echo Received trapped signal, beginning shutdown...;" KILL TERM HUP INT EXIT;

echo NiFi running with PID ${nifi_pid}.
wait ${nifi_pid}
