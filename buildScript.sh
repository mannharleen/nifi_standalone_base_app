#!/bin/bash

##### USAGE
# sudo ./buildScript.sh          --> this will create a new image using the flow.xml.gz in the current folder
# sudo ./buildScript.sh y        --> this will copy flow.xml and create a new image
# sudo ./buildScript.sh y  1.0.x --> this will copy flow.xml and create a new image, push to ECR using the version provided
# sudo ./buildScript.sh n  1.0.x --> this will create a new image using the flow.xml.gz in the current folder, push to ECR using the version provided
#


#if [ -z "$*" ]; then echo "ERROR: Please provide version as argument! "; exit -9; fi

export COPY_FROM_MYNIFI=$1
export VERSION=$2

if [ "$COPY_FROM_MYNIFI" == "y" ]; then
	echo "*************** Copying flow.xml from container named mynifi"
	sudo docker cp [containername]:/opt/nifi/nifi-current/conf/flow.xml.gz /root/
	sudo cp /root/flow.xml.gz .
else
	echo "************** Using flow.xml from current location"

fi

echo "************** Building docker image named [xxx]"
docker build -t [image_name_tag] .

if [ -n "$VERSION" ]; then
	sudo yum install aws-cli -y || sudo apt-get install awscli -y
	$(aws ecr get-login --no-include-email --region ap-southeast-2)
	#docker tag hbv2_standalone_cnimeterdataapi:latest 920534304737.dkr.ecr.ap-southeast-2.amazonaws.com/hbv2_standalone_cnimeterdataapi:$VERSION
	#docker push 920534304737.dkr.ecr.ap-southeast-2.amazonaws.com/hbv2_standalone_cnimeterdataapi:$VERSION
	#docker tag hbv2_standalone_cnimeterdataapi:latest 920534304737.dkr.ecr.ap-southeast-2.amazonaws.com/hbv2_standalone_cnimeterdataapi:latest
	#docker push 920534304737.dkr.ecr.ap-southeast-2.amazonaws.com/hbv2_standalone_cnimeterdataapi:latest
	#echo "************** success: pushed to ECR"
fi
echo "************** success: overall"
